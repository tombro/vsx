var net = require('net');
var io = require('socket.io').listen(3200);

var HOST = '192.168.1.117';
var PORT = 8102;

var connect = require('connect');
var serveStatic = require('serve-static');
connect().use(serveStatic(__dirname)).listen(8080);

var client = new net.Socket();
var volume;

client.connect(PORT, HOST, function() {
    console.log('CONNECTED TO: ' + HOST + ':' + PORT);
});

io.sockets.on('connection', function (socket) {
	client.write("?GAP\r");

	socket.on('CMD',function(data){
		client.write(data+"\r");
	})

	socket.on('sexytime',function(){
		changeVolume(57);
		toSexyTime();
	})

	socket.on('stubru',function(){
		changeVolume(31);
		toStuBru();
	})
  
});

client.on('connect',function(){
    client.write("?FN\r");
});

// Add a 'data' event handler for the client socket
// data is what the server sent to this socket
client.on('data', function(data) {
	var song = "";
	var artist = "";
    var the_data = String(data);
    the_data = the_data.split("\n");
    for(var item in the_data) { 
    	if(the_data[item].indexOf("GEP02") > -1){
    		if(the_data[item] != "" && the_data[item] != "\n" && the_data[item] != " "){
	    		song = extractText(the_data[item]);
	    		console.log("!"+the_data[item]+"!");
				io.sockets.emit("changeSong",song);
    		}
		}
    	if(the_data[item].indexOf("GEP01") > -1){
    		if(the_data[item] != "" && the_data[item] != "\n" && the_data[item] != " "){
	    		artist = extractText(the_data[item]);
	    		console.log("!"+the_data[item]+"!");
				io.sockets.emit("changeArtist", artist);
    		}
		}

	}
	if(isChannelChange(String(data))){
    	client.write("?VOL\r");
		switch(String(data).replace(/(\r\n|\n|\r)/gm,"")){
			case "FN33":
				console.log("adapter");
				changeVolume(31);
				toStuBru();
				break;
			case "FN15":
				console.log("DVRBDR");
				changeVolume(57);
				toSexyTime();
				break;
			case "FN06":
				console.log("tv");
				changeVolume(81);
				break;
			case "FN44":
				console.log("mediaserver");
				changeVolume(57);
				break;
			case "FN45":
				console.log("radio");
				changeVolume(31);
				break;
			default:
				break;
		}
	}
	if(isVolume(String(data))){
		volume = extractVolume(String(data));
		console.log("volume is: " + volume);
	}
	
	io.sockets.emit("fb",String(data));
	//client.write('PF');
    // Close the client socket completely
    client.ref();
    
});

client.on('error',function(data){
	console.log("Error: " + data);
	})

// Add a 'close' event handler for the client socket
client.on('close', function() {
    console.log('Connection closed');
});

function toStuBru(){
	powerOn();
	sleep(1000, function() {
		client.write("45FN\r");
		client.write("?FN\r");
	});
	sleep(2500,function(){
		client.write("00001GHP\r");
	})
}
function toSexyTime(){
	powerOn();
	
	sleep(1000, function() {
		client.write("44FN\r");
		client.write("?FN\r");
	});
	sleep(2500,function(){
		client.write("00003GHP\r");
	})
	sleep(3000, function() {
	   client.write("00001GHP\r");
	})
	sleep(3500, function() {
		client.write("00003GHP\r");	
	});
	sleep(4000, function() {
		client.write("00005GHP\r");
	});
	sleep(4500, function() {
		client.write("00001GHP\r");
	});
}

function extractText( str ){
  var ret = "";

  if ( /"/.test( str ) ){
    ret = str.match( /"(.*?)"/ )[1];
  } else {
    ret = str;
  }

  return ret;
}

function isVolume(str){
  var pat = new RegExp( "^VOL[0-9]{3}$" );
  str = str.replace(/(\r\n|\n|\r)/gm,"");

  if ( pat.test( str )){
    return true;
  } else {
    return false;
  }
}

function isChannelChange(str){
  var pat = new RegExp( "^FN[0-9]{2}$" );
  str = str.replace(/(\r\n|\n|\r)/gm,"");

  if ( pat.test( str )){
    return true;
  } else {
    return false;
  }
}

function changeVolume(desiredVolume){
	times = (desiredVolume-volume)/2;
	if (times > 0) {
		for (var i = 0; i < times; i++) {
			client.write("VU\r");
		}
	}
	else{
		for (var i = 0; i < Math.abs(times); i++) {
			client.write("VD\r");
		}
	}
	volume = desiredVolume;
}

function extractVolume( str ){
  var ret = "";
  var pat = new RegExp( "^[A-Za-z]{3}[0-9]{3}$" );
  str = str.replace(/(\r\n|\n|\r)/gm,"");

  if ( pat.test( str )){
    ret = str.replace("VOL","");
  } else {
    ret = str;
  }

  return ret;
}

function powerOn(){
	client.write("PO\r");
}

function sleep(time, callback) {
    var stop = new Date().getTime();
    while(new Date().getTime() < stop + time) {
    }
    callback();
}
